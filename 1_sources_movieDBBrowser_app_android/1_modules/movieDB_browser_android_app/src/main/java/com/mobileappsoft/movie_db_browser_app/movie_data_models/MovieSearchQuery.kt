package com.mobileappsoft.movie_db_browser_app.movie_data_models

data class MovieSearchQuery(val movieName: String)
