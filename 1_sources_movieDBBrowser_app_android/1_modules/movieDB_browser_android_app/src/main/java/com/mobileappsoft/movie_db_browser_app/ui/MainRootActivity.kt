package com.mobileappsoft.movie_db_browser_app.ui

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.mobileappsoft.movie_db_browser_app.R
import com.mobileappsoft.movie_db_browser_app.ui.search_movie_screen.SearchMovieFragment

class MainRootActivity: AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.search_movie_main_activity)
        if (savedInstanceState == null) {
            supportFragmentManager.beginTransaction()
                .replace(R.id.container, SearchMovieFragment.newInstance())
                .commitNow()
        }
    }

}
