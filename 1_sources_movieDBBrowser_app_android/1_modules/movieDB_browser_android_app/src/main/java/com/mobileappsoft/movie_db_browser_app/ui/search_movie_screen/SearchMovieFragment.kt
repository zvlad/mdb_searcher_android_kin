package com.mobileappsoft.movie_db_browser_app.ui.search_movie_screen

import androidx.lifecycle.ViewModelProviders
import android.os.Bundle
import android.view.*
import androidx.appcompat.widget.SearchView
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.mobileappsoft.movie_db_browser_app.R
import com.mobileappsoft.movie_db_browser_app.movie_data_models.MovieSearchQuery
import com.mobileappsoft.movie_db_browser_app.movie_search_behavior.SearchMovieListDataSourceFactory
import com.mobileappsoft.zandroidframework.android.FragmentWithViewModel
import kotlinx.android.synthetic.main.search_movie_main_fragment.*

class SearchMovieFragment: FragmentWithViewModel<SearchMovieScreenViewModel>() {

    companion object {
        fun newInstance() = SearchMovieFragment()
    }

    private lateinit var viewModel: SearchMovieScreenViewModel

    private val moviesListView by lazy {
        moviesListViewX
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        setHasOptionsMenu(true)
        return inflater.inflate(R.layout.search_movie_main_fragment, container, false)
    }

    private lateinit var moviesListAdapter: MoviesListAdapter

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        setupMoviesListViewLayout()
        viewModel = mainViewModel()
        moviesListAdapter = MoviesListAdapter(viewModel)
        moviesListView.adapter = moviesListAdapter
        viewModel.moviesList.observe(this@SearchMovieFragment, Observer {
            moviesListAdapter.submitList(it)
        })
    }

    override fun mainViewModel(): SearchMovieScreenViewModel {
        return ViewModelProviders.of(this,
            SearchMovieScreenViewModel.Factory(
                SearchMovieListDataSourceFactory()
            )).get(SearchMovieScreenViewModel::class.java)
    }

    private fun setupMoviesListViewLayout() {
        val linearLayoutManager = LinearLayoutManager(
            activity, RecyclerView.VERTICAL,
            false
        )
        moviesListView.layoutManager = linearLayoutManager
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        super.onCreateOptionsMenu(menu, inflater)
        activity?.menuInflater?.inflate(R.menu.main_menu, menu)

        val menuSearch = menu.findItem(R.id.action_search)
        val mSearchView = menuSearch.actionView as SearchView
        mSearchView.queryHint = "Search"

        mSearchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String?): Boolean {
                return false
            }

            override fun onQueryTextChange(newText: String): Boolean {
                viewModel.onSearchQueryChanged(
                    MovieSearchQuery(
                        newText
                    )
                )
                return true
            }
        })
    }

}
