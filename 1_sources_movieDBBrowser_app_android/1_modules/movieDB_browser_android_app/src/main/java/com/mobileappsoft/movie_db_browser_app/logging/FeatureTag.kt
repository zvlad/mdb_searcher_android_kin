package com.mobileappsoft.movie_db_browser_app.logging

object FeatureTag {
    const val moviesListUI = "MoviesListUI"
    const val moviesListSearchRequest = "MoviesListSearchRequest"
    const val initialization = "Initialization"
}
