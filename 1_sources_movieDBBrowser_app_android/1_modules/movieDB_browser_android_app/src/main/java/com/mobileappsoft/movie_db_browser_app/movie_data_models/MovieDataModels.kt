package com.mobileappsoft.movie_db_browser_app.movie_data_models

data class Movie(
    val id: Long,
    val name: String,
    val year: String,
    val description: String,
    val posterUrl: String
)

data class MoviesPage(
    var page: Long = 0,
    var total_results: Long = 0,
    var total_pages: Long = 0,
    var results: ArrayList<MovieRemoteAPIDTO>
)

data class MovieRemoteAPIDTO(
    var vote_count: Long = 0,
    var id: Long = 0,
    var title: String = "",
    var poster_path: String = "",
    var backdrop_path: String = "",
    var overview: String = "",
    var release_date: String = "",
    var vote_average: Float = 0.toFloat()
)

