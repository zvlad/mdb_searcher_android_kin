package com.mobileappsoft.movie_db_browser_app.ui.search_movie_screen

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.paging.PagedList
import com.mobileappsoft.movie_db_browser_app.logging.FeatureTag
import com.mobileappsoft.movie_db_browser_app.movie_data_models.Movie
import com.mobileappsoft.movie_db_browser_app.movie_data_models.MovieSearchQuery
import com.mobileappsoft.movie_db_browser_app.movie_search_behavior.SearchMovieListDataSourceFactory
import com.mobileappsoft.zandroidframework.android.logger.logDebug

class SearchMovieScreenViewModel(
    private val moviesListDataSourceFactory: SearchMovieListDataSourceFactory
)
    : ViewModel() {

    companion object {
        private const val initialSearchQuery = "Matrix"
    }

    private var searchResults =
        MutableLiveData<PagedList<Movie>>().apply {
        moviesListDataSourceFactory.startSearch(initialSearchQuery).observeForever {
            this.value = it
        }
    }

    val moviesList: LiveData<PagedList<Movie>> = searchResults

    fun onSearchQueryChanged(query: MovieSearchQuery) {                                     logDebug(
        FeatureTag.moviesListUI, "new searchQuery: ${query.movieName}")
        if (query.movieName.isEmpty())
            return

        moviesListDataSourceFactory.startSearch(query.movieName).observeForever {
            searchResults.value = it
        }
    }

    fun onMovieClicked(movie: Movie) {                                                      logDebug(
        FeatureTag.moviesListUI, "Selected ${movie.name}")
    }

    class Factory(
        private val movieFeedDataSourceFactory: SearchMovieListDataSourceFactory
    ): ViewModelProvider.Factory {

        override fun <T: ViewModel?> create(modelClass: Class<T>): T {
            val vm = SearchMovieScreenViewModel(
                movieFeedDataSourceFactory
            )
            return vm as T
        }
    }
}