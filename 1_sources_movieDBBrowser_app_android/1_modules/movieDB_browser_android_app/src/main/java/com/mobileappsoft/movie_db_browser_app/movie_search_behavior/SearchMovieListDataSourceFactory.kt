package com.mobileappsoft.movie_db_browser_app.movie_search_behavior

import androidx.lifecycle.LiveData
import androidx.paging.DataSource
import androidx.paging.LivePagedListBuilder
import androidx.paging.PageKeyedDataSource
import androidx.paging.PagedList
import com.mobileappsoft.movie_db_browser_app.logging.FeatureTag
import com.mobileappsoft.movie_db_browser_app.movie_data_models.Movie
import com.mobileappsoft.movie_db_browser_app.movie_data_models.MoviesPage
import com.mobileappsoft.zandroidframework.android.logger.logDebug
import com.mobileappsoft.zandroidframework.android.logger.logDebugEnteredMethod
import com.mobileappsoft.zandroidframework.network_client.RetrofitNetworkClientFactory
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.*
import java.util.concurrent.Executors

class SearchMovieListDataSourceFactory: DataSource.Factory<Int, Movie>() {
    private var searchQuery = ""

    override fun create(): DataSource<Int, Movie> {
        return SearchMovieListDataSource(
            searchQuery
        )
    }

    fun startSearch(newSearchQuery: String): LiveData<PagedList<Movie>> {
        this.searchQuery = newSearchQuery
        val pageSize = 20
        val config = PagedList.Config.Builder()
            .setPageSize(pageSize)
            .setPrefetchDistance(1)
            .setInitialLoadSizeHint(pageSize)
            .setEnablePlaceholders(false)
            .build()
        val executor = Executors.newFixedThreadPool(5)
        return LivePagedListBuilder(this, config)
            .setFetchExecutor(executor)
            .build()
    }

    private class SearchMovieListDataSource(private val searchQuery: String)
        : PageKeyedDataSource<Int, Movie>() {

        private val runningSearchRequests: ArrayList<Call<MoviesPage>> = arrayListOf()

        init {
            logDebugEnteredMethod(FeatureTag.initialization)
        }

        private val movieSearchService: SearchMovieService by lazy {
            RetrofitNetworkClientFactory().getNetworkClient(SearchMovieService.baseURL)
                .create(SearchMovieService::class.java)
        }

        override fun loadInitial(
            params: LoadInitialParams<Int>,
            callback: LoadInitialCallback<Int, Movie>) {

            val loadPageCallback =
                LoadPageCallbackAdapter(
                    loadInitialPageCallback = callback,
                    nextPageIndex = 2
                )
            requestPageFromApi(1, convertToApiRequestCallback(loadPageCallback))
        }

        override fun loadAfter(params: LoadParams<Int>,
                               callback: LoadCallback<Int, Movie>) {
            val currentPageIndex = params.key
            val loadPageCallback =
                LoadPageCallbackAdapter(
                    loadRegularPageCallback = callback,
                    nextPageIndex = currentPageIndex + 1
                )
            requestPageFromApi(currentPageIndex, convertToApiRequestCallback(loadPageCallback))
        }

        private fun requestPageFromApi(pageIndex: Int, callback: Callback<MoviesPage>) {        logDebugEnteredMethod(
            FeatureTag.moviesListSearchRequest)
            logDebug(
                FeatureTag.moviesListSearchRequest, "searchQuery == <$searchQuery>")
            val searchRequest = movieSearchService.getSearchResult(
                SearchMovieService.apiKey,
                searchQuery,
                pageIndex
            )
            runningSearchRequests.add(searchRequest)
            searchRequest.enqueue(callback)
        }

        private class LoadPageCallbackAdapter(
            private val loadInitialPageCallback: LoadInitialCallback<Int, Movie>? = null,
            private val loadRegularPageCallback: LoadCallback<Int, Movie>? = null,
            private val nextPageIndex: Int) {

            fun onResult(data: List<Movie>) {
                loadInitialPageCallback?.onResult(data, null, nextPageIndex)
                loadRegularPageCallback?.onResult(data, nextPageIndex)
            }
        }

        private fun convertToApiRequestCallback(callback: LoadPageCallbackAdapter): Callback<MoviesPage> {
            return object: Callback<MoviesPage> {
                override fun onResponse(
                    call: Call<MoviesPage>,
                    response: Response<MoviesPage>
                ) {
                    val movies = convertMoviesListResponse(response)
                    movies?.let {
                        callback.onResult(it)
                    }

                }

                override fun onFailure(call: Call<MoviesPage>, t: Throwable) {
                    logDebugEnteredMethod(FeatureTag.moviesListSearchRequest)
                }

            }
        }

        private fun convertMoviesListResponse(response: Response<MoviesPage>): List<Movie>? {
            return response.body()?.results
                ?.map {
                    Movie(
                        it.id,
                        it.title,
                        it.release_date,
                        it.overview,
                        /* TODO: 4/16/19 @VladZamskoi: poster loading Implementation */
                        "cwBq0onfmeilU5xgqNNjJAMPfpw.jpg"
                    )
                }
        }

        override fun loadBefore(params: LoadParams<Int>, callback: LoadCallback<Int, Movie>) {
            logDebugEnteredMethod(FeatureTag.moviesListSearchRequest)
        }
    }
}