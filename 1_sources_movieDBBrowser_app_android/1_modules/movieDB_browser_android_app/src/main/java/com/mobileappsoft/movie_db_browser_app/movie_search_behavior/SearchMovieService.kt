package com.mobileappsoft.movie_db_browser_app.movie_search_behavior

import com.mobileappsoft.movie_db_browser_app.movie_data_models.MoviesPage
import com.mobileappsoft.movie_db_browser_app.secret_config.theMovieDBKey
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query

interface SearchMovieService {

    companion object {
        const val apiKey: String = theMovieDBKey
        const val baseURL: String = "https://api.themoviedb.org/3/"
        const val postersURL: String = "https://image.tmdb.org/t/p/w500/"
    }

    @GET("search/movie")
    fun getSearchResult(
        @Query("api_key") api_key: String,
        @Query("query") query: String,
        @Query("page") page: Int)
            : Call<MoviesPage>
}