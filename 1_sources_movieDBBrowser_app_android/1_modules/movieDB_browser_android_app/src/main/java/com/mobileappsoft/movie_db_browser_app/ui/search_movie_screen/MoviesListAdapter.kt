package com.mobileappsoft.movie_db_browser_app.ui.search_movie_screen

import android.view.View
import android.view.ViewGroup
import androidx.paging.PagedListAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.mobileappsoft.movie_db_browser_app.movie_data_models.Movie
import com.mobileappsoft.movie_db_browser_app.R
import com.mobileappsoft.zandroidframework.android.inflate
import kotlinx.android.synthetic.main.movie_list_item_layout.view.*

class MoviesListAdapter(private val viewModel: SearchMovieScreenViewModel)
    : PagedListAdapter<Movie, MoviesListAdapter.MovieItemViewHolder>(MovieItemComparator()) {

    override fun onBindViewHolder(viewHolder: MovieItemViewHolder, position: Int) {
        getItem(position)?.let {
            viewHolder.bind(it)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MovieItemViewHolder {
        return MovieItemViewHolder(parent.inflate(R.layout.movie_list_item_layout))
    }

    inner class MovieItemViewHolder(itemView: View): RecyclerView.ViewHolder(itemView) {
        private val movieItemContainerView by lazy {
            itemView.movieItemContainerViewX
        }

        private val nameView by lazy {
            itemView.nameViewX
        }

        private val posterView by lazy {
            itemView.posterViewX
        }

        private val descriptionView by lazy {
            itemView.descriprionViewX
        }

        private val releaseDateView by lazy {
            itemView.releaseDateViewX
        }

        fun bind(movie: Movie) {
            nameView.text = movie.name
            releaseDateView.text = movie.year.toString()
            descriptionView.text = movie.description
            movieItemContainerView.setOnClickListener {
                viewModel.onMovieClicked(movie)
            }
        }
    }

    class MovieItemComparator: DiffUtil.ItemCallback<Movie>() {
        override fun areItemsTheSame(oldItem: Movie, newItem: Movie) =
            oldItem.id == newItem.id

        override fun areContentsTheSame(oldItem: Movie, newItem: Movie) =
            oldItem == newItem
    }


}

