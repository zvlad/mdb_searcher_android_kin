package com.mobileappsoft.zandroidframework.android.logger

private const val kotlinCallerStackTraceIndex = 5

private fun log(message: String,
                tag: String = "",
                logFunc: ((t: String, m: String) -> Int) = ZLog::d,
                stackTraceEntryIndex: Int = kotlinCallerStackTraceIndex
) {
    val stackTraceEntry = Thread.currentThread().stackTrace[stackTraceEntryIndex]
    val fileName = stackTraceEntry.fileName
    val methodName = stackTraceEntry.methodName
    val lineNumber = stackTraceEntry.lineNumber
    val fullPrefix = "${if (tag.isNotEmpty()) "$tag " else ""}($fileName:$lineNumber) $methodName()"
    logFunc(fullPrefix, message)
}

/**
 * Do not change signature of this method. It's been introduced for compatibility with
 * existing Java code.
 * E.g. `Log.d(TAG, "message")`
 * */
fun logDebug(tag: String, message: String) {
    log(
        message = message,
        tag = tag,
        logFunc = ZLog::d
    )
}

fun logDebug(message: String) {
    log(
        message = message,
        tag = "None",
        logFunc = ZLog::d
    )
}

fun logDebugEnteredMethod(tag: String) {
    log(
        message = "Entered",
        tag = tag,
        logFunc = ZLog::d
    )
}


/**
 * Do not change signature of this method. It's been introduced for compatibility with
 * existing Java code.
 * E.g. `Log.i(TAG, "message")`
 * */
fun logInfo(tag: String, message: String) {
    log(
        message = message,
        tag = tag,
        logFunc = ZLog::i
    )
}

fun logInfo(message: String) {
    log(
        message = message,
        tag = "",
        logFunc = ZLog::i
    )
}


/**
 * Do not change signature of this method. It's been introduced for compatibility with
 * existing Java code.
 * E.g. `Log.w(TAG, "message")`
 * */
fun logWarning(tag: String, message: String) {
    log(
        message = message,
        tag = tag,
        logFunc = ZLog::w
    )
}

fun logWarning(message: String) {
    log(
        message = message,
        tag = "",
        logFunc = ZLog::w
    )
}


/**
 * Do not change signature of this method. It's been introduced for compatibility with
 * existing Java code.
 * E.g. `Log.e(TAG, "message")`
 * */
fun logError(tag: String, message: String) {
    log(
        message = message,
        tag = tag,
        logFunc = ZLog::e
    )
}

fun logErrorWithDefaultMessage(tag: String) {
    log(
        message = "Error occurred",
        tag = tag,
        logFunc = ZLog::e
    )
}

fun logError(message: String) {
    log(
        message = message,
        tag = "",
        logFunc = ZLog::e
    )
}

